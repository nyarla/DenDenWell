DenDenWell - A DenDenMarkdown Previewer for TextWell
====================================================

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy?template=https://github.com/nyarla/DenDenWell)

How to use
----------

  1. Access to <https://dendenwell.herokuapp.com/>
  2. Install TextWell Action
  3. Write [DenDenMarkdown](http://conv.denshochan.com/markdown)
     * NOTE: this documentation is Japanese
  4. Do **DenDenWell** TextWell's Action!

Author
------

  * Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

UNLICENSE
---------

Unless otherwise noted, this package is under the public domain.

This package is built onto these libraries:

  1. <https://github.com/silexphp/Silex> (MIT)
  2. <https://github.com/bobthecow/mustache-silex-provider> (MIT)
  3. <https://github.com/symfony/symfony> (MIT)
  4. <https://github.com/symfony/Form> (MIT)
  5. <https://github.com/symfony/Security> (MIT)
  6. <https://github.com/symfony/security-csrf> (MIT)
  7. <https://github.com/michelf/php-markdown> (BSD-3-Clause)
  8. <https://github.com/denshoch/DenDenMarkdown> (BSD-3-Clause)

