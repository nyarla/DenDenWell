<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../lib/DenDenMarkdown.php';

use Denshoch\DenDenMarkdown;
use Silex\Application;
use Silex\Provider\FormServiceProvider;
use Symfony\Component\HttpFoundation\Request;

$md  = new DenDenMarkdown();
$md->emtry_element_suffix   = " >";
$md->no_markup              = true;
$md->no_entities            = true;
$md->url_filter_func        = function ($url) {
    if ( preg_match("/^(http|https|mailto):/", $url) ) {
        return htmlspecialchars( $url, ENT_QUOTES | ENT_DISALLOWED | ENT_HTML5, 'UTF-8' );
    } else {
        return "";
    }
};

$secret = getenv('DENDENWELL_CSRF_SECRET') || die("please set DENDENWELL_CSRF_SECRET");

$app = new Application();
$app->register( new Mustache\Silex\Provider\MustacheServiceProvider, array(
    'mustache.path' => __DIR__.'/../templates',
) );

$app->register(new FormServiceProvider());
$app['form.secret'] = md5( $secret, uniqid(rand(), true) );

$app->get('/', function () use ($app) {
    return $app['mustache']->render('install', array());
});

$app->get('/form', function () use ($app) {
    return $app['mustache']->render('form', array( 'token' => $app['form.csrf_provider']->generateCsrfToken('csrf_token') ) );
});

$app->post('/render', function (Request $req) use($app, $md) {
    $post_token = $req->get('token');

    if ( $app['form.csrf_provider']->isCsrfTokenValid('csrf_token', $post_token) ) {
        return $app['mustache']->render('render', array( 'content' => $md->transform($req->get('body')) ));
    } else {
        $app->abort(403);
    }
});

$app->run();

